#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>
using namespace std;

//数组(vector)对数器
//1.数组转vector
vector<int> ArrayToVector(int *arr,int len){
    vector<int> v;
    for(int i = 0;i < len;i++){
        v.push_back(arr[i]);
    }
    return v;
}

//vector转数组
int* VectorToArray(vector<int> v,int len){
    int *arr = new int[len];
    for(int i = 0;i < len;i++){
        arr[i] = v[i];
    }
    return arr;
}

//2.产生随机数组，srand不能放在此函数中，否则导致一秒内产生的数组相同
vector<int> randomArray(int maxValue,int maxSize){
    vector<int> v;
    for(int i = 0;i < maxSize;i++){
        v.push_back(rand()%maxValue);
    }
    return v;
}

//3.比较两个输出数组是否相等
bool Compare(vector<int> v1,vector<int> v2){
    if(v1.empty() && v2.empty())
        return true;
    if((v1.empty() && !v2.empty()) || (!v1.empty() && v2.empty()))
        return true;
    if(v1.size() != v2.size())
        return false;
    for(int i = 0;i < v1.size();i++){
        if(v1[i] != v2[i])
            return false;
    }
    return true;
}

//4.调用函数
void algorithmTest(int time = 500,int maxValue = 5000,int maxSize = 100){
    for(int i = 0;i < time;i++){
        vector<int> v1 = randomArray(maxValue,maxSize);
        vector<int> v2(v1);

        //系统算法对v1操作
        sort(v1.begin(),v1.end());
        
        //自己写的算法对V2操作
        
        
        if(!Compare(v1,v2)){
            cout << "error" << endl;
            break;
        }
    }
    cout << "right" << endl;
}


int main(){
    srand((unsigned)time(NULL));
    algorithmTest();
    return 0;
}
