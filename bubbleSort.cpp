#include <iostream>
using namespace std;



void swap(int &a,int &b){
    int temp = a;
    a = b;
    b = temp;
}


void bubbleSort(int arr[],int length){
    for(int i = length - 1;i > 0;i--){
        for(int j = 0;j < i;j++){
            if(arr[j] > arr[j+1]){
                swap(arr[j],arr[j+1]);
            }            
        }
    }
}

void bubbleSort2(int arr[],int length){
    for(int i = 0;i < length - 1;i++){
        for(int j = 0;j < length-i-1;j++){
            if(arr[j] > arr[j+1]){
                swap(arr[j],arr[j+1]);
            }
        }
    }
}


void printArray(int arr[],int length){
    for(int i = 0; i < length;i++){
        cout << arr[i] << " ";
    }
    cout << endl;
}
int main(){
    int arr[] = {2,4,2,14,66,3,21,66,6};
    int length = sizeof(arr)/sizeof(arr[0]);
    
    printArray(arr,length);

    bubbleSort2(arr,length);

    printArray(arr,length);

    return 0;
}
