#include <iostream>
using namespace std;

//递归查询数组中的最大数
int findMax(int arr[],int L,int R){
    if(L == R)
        return arr[L];

    int mid = L + ((R - L) >> 1);
    int leftMax = findMax(arr,L,mid);
    int rightMax = findMax(arr,mid+1,R);
    return max(leftMax,rightMax);
}

int main(){
    int arr[] = {1,3,4,5,6,23,4,5543,343,244};
    int len = sizeof(arr)/sizeof(arr[0]);
    cout << findMax(arr,0,len-1) << endl;
    return 0;
}
