#include <iostream>
using namespace std;

int arr[] = {12,6,89,663,2,4,3,5,7,9,222,5,78,999,44};
const int len = sizeof(arr)/sizeof(int);
int b[10][len + 1] = {0};
void swap(int &a,int &b);
void printArray(int arr[],int length);
int numOfDigits(int arr[]);
void distributeElement(int arr[],int b[10][len+1],int digits);
void collectElement(int arr[],int b[10][len+1]);
void zeroBucket(int b[][len+1]);
void bucketSort(int arr[]);

int main(){
    printArray(arr,len);
    bucketSort(arr);
    printArray(arr,len);        
    return 0;
}

void swap(int &a,int &b){
    int temp = a;
    a = b;
    b = temp;
}
void printArray(int arr[],int length){
    for(int i = 0;i < length;i++)
        cout << arr[i] << " ";
    cout << endl;
}

//获取最大值位数
int numOfDigits(int arr[]){
    int largest = 0;
    for(int i = 0;i < len;i++){//获取最大值
        if(arr[i] > largest)
            largest = arr[i];
    }
    int digits = 0;//最大值的位数
    while(largest){
        digits++;
        largest /= 10;
    }
    return digits;
}

void distributeElement(int arr[],int b[10][len+1],int digits){
    int divisor = 10;//除数
    for(int i = 1;i < digits;i++){
        divisor *= 10;
    }
    for(int j = 0; j < len;j++){
        //numOfDigist为相应的（divisor/10）位上的值
        int numOfDigist = (arr[j]%divisor - arr[j]%(divisor/10)) / (divisor/10);
        int num = ++b[numOfDigist][0];//b中第一列元素来存储每行中元素的个数
        b[numOfDigist][num] = arr[j];
    }
}

void collectElement(int arr[],int b[10][len+1]){
    int k = 0;
    for(int i = 0;i < 10;i++){
        for(int j = 1;j < b[i][0];j++){
            arr[k++] = b[i][j];
        }
    }
}
//将b中元素全部置0
void zeroBucket(int b[][len+1]){
    for(int i = 0;i < 10;i++){
        for(int j = 0;j < len+1;j++){
            b[i][j] = 0;
        }
    }
}

void bucketSort(int arr[]){
    int digits = numOfDigits(arr);
    for(int i = 1;i <= digits;i++){
        distributeElement(arr,b,i);
        collectElement(arr,b);
        if(i != digits){
            zeroBucket(b);
        }
    }
}


