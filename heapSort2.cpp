#include <iostream>
using namespace std;

void swap(int &a,int &b){
    int temp = a;
    a = b;
    b = temp;
}

void printArray(int arr[],int length){
    for(int i = 0;i < length;i++)
        cout << arr[i] << " ";
    cout << endl;
}

void maxHeapfiy(int arr[],int start,int end){
    //建立父节点和子节点指标
    int dad = start;
    int son = dad * 2 + 1;
    while(son <= end){//子节点在范围内
        if(son + 1 <= end && arr[son] < arr[son+1])//比较两个子节点大小，选择最大的
            son++;
        if(arr[dad] > arr[son]) 
            return;//父节点大于子节点，调整完毕，退出函数
        else{//否则交换父子内容再继续子节点和孙节点比较
            swap(arr[dad],arr[son]);
            dad = son;
            son = dad * 2 + 1;
        }
    }
}


void heapSort(int arr[],int length){
    //初始化，i从最后一个父节点开始调整
    for(int i = length/2 -1;i >= 0;i--){
        maxHeapfiy(arr,i,length-1);
    }
    //现将第一个元素和已经排好的元素前一位做交换，再重新调整，直到排序完成
    for(int i = length - 1;i > 0;i--){
        swap(arr[0],arr[i]);
        maxHeapfiy(arr,0,i - 1);
    }
}


int main(){
    int arr[] = {12,6,89,663,2,4,3,5,7,9,222,5,78,999,44};
    int length = sizeof(arr)/sizeof(arr[0]);

    printArray(arr,length);
    heapSort(arr,length);
    printArray(arr,length);
        
    return 0;
}
