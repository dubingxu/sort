#include <iostream>
using namespace std;

void swap(int &a,int &b){
    int temp = a;
    a = b;
    b = temp;
}

void printArray(int arr[],int length){
    for(int i = 0;i < length;i++)
        cout << arr[i] << " ";
    cout << endl;
}


void quickSort(int arr[],int L,int R){
    if(R <= L) return;
    int i = L;
    int j = R + 1;
    int key = arr[L];
    while(true){
        //从左往右找比key大的值
        while(arr[++i] < key){
            if(i == R)
                break;
        }
        //从右往左找比key小的值
        while(arr[--j] > key){
            if(j == L)
                break;
        }

        if( i >= j) break;
        swap(arr[i],arr[j]);
    }
    //参考值与j对应值进行交换
    arr[L] = arr[j];
    arr[j] = key;
    quickSort(arr,L,j-1);//<区
    quickSort(arr,j+1,R);//>区
}

void quickSortC(int arr[],int L,int R){
    if(L >= R) return;
    int i = L;
    int j = R;
    int key = arr[L];

    while(i < j){
        while(i < j && key <= arr[j])
            j--;
        swap(arr[i],arr[j]);

        while(i < j && key >= arr[i])
            i++;
        swap(arr[i],arr[j]);
    }
    quickSortC(arr,L,i-1);
    quickSortC(arr,i+1,R);
}


int main(){
    int arr[] = {12,6,89,663,2,4,3,5,7,9,222,5,78,999,44};
    int length = sizeof(arr)/sizeof(arr[0]);

    printArray(arr,length);
    quickSortC(arr,0,length-1);
    printArray(arr,length);
        
    return 0;
}
