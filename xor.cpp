#include <iostream>
using namespace std;

void project1(int arr[],int length){
    int eor = 0;
    for(int i = 0;i < length;i++){
        eor ^= arr[i];
    }
    cout << "出现奇数次的数为： " << eor << endl;
}


void project2(int arr[],int length){
    int eor = 0;
    for(int i = 0;i < length;i++){
        eor ^= arr[i];
    }

    int rightOne = eor & (~eor + 1);
    int onlyOne = 0;
    for(int i = 0;i < length;i++){
        if(arr[i]&rightOne == 1){
            onlyOne ^= arr[i];
        }
    }

    cout << "出现奇数词的两个数为：" << onlyOne << " " << (onlyOne ^ eor) << endl;
}

void printArray(int arr[],int len){
    for(int i= 0;i < len;i++){
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main(){
    int arr[] = {1,2,3,4,5,5,4,3,2,1,0};
    int length = sizeof(arr)/sizeof(arr[0]);
    printArray(arr,length);
    project1(arr,length);

    int arr2[] = {1,2,3,4,5,5,4,3,2,1,0,1,0,2};
    length = sizeof(arr2)/sizeof(arr2[0]);
    printArray(arr2,length);
    
    project2(arr2,length);

    return 0;
}
