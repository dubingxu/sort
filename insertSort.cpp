#include <iostream>
using namespace std;

void swap(int &a,int &b){
    int temp = a;
    a = b;
    b = temp;
}

void insertSort(int arr[],int len){
    for(int i = 1;i < len; i++){
        for(int j = i-1;j > 0 && arr[j] > arr[j+1];j--){
            swap(arr[j],arr[j+1]);
        }
    }
}

void printArray(int arr[],int len){
    for(int i = 0;i < len;i++){
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main(){
    int arr[] = {3,4,2,4,52,34,5,6,2,2,5,56};
    int len = sizeof(arr)/sizeof(arr[0]);
    printArray(arr,len);

    insertSort(arr,len);
    printArray(arr,len);
    return 0;
}
