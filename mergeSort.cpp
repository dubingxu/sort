#include <iostream>
using namespace std;

void swap(int &a,int &b){
    int temp = a;
    a = b;
    b = temp;
}

void printArray(int arr[],int length){
    for(int i = 0;i < length;i++)
        cout << arr[i] << " ";
    cout << endl;
}

void merge(int arr[],int L,int M,int R){
    int num = R-L+1;
    int *help = new int[R-L+1]; 
    int i = 0;
    int p1 = L;
    int p2 = M + 1;
    while(p1 <= M && p2 <= R){
        help[i++] = arr[p1] <= arr[p2]?arr[p1++]:arr[p2++];
    }
    while(p1 <= M){
        help[i++] = arr[p1++];
    }
    while(p2 <= R){
        help[i++] = arr[p2++];
    }

    for(int i = 0;i < R-L+1;i++){
        arr[L+i] = help[i];
    }
}

void mergeSort(int arr[],int L,int R){
    if(L == R)
        return ;
    int mid = L + ((R - L) >> 1);
    mergeSort(arr,L,mid);
    mergeSort(arr,mid+1,R);
    merge(arr,L,mid,R);
}

int main(){
    int arr[] = {12,6,89,663,2,4,3,5,7,9,222,5,78,999,44};
    int length = sizeof(arr)/sizeof(arr[0]);

    printArray(arr,length);
    mergeSort(arr,0,length-1);
    printArray(arr,length);
        
    return 0;
}
