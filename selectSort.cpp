#include <iostream>
using namespace std;

void swap(int &a,int &b){
    int temp = a;
    a = b;
    b = temp;
}


void selectSort(int arr[],int length){
    for(int i = 0;i < length - 1;i++){
        int minIndex = i;
        for(int j = i + 1;j < length;j++){
            minIndex = arr[j] < arr[minIndex]?j:minIndex;
        }
        swap(arr[i],arr[minIndex]);
    }
}


void printArray(int arr[],int length){
    for(int i = 0; i < length;i++){
        cout << arr[i] << " ";
    }
    cout << endl;
}
int main(){
    int arr[] = {2,4,2,14,66,3,21,66,6};
    int length = sizeof(arr)/sizeof(arr[0]);
    
    printArray(arr,length);

    selectSort(arr,length);
    printArray(arr,length);

    return 0;
}
